FROM ubuntu:18.04

# Set default shell to bash
SHELL ["/bin/bash", "-c"] 

# Expose Ports
EXPOSE 8080

WORKDIR /opt/jenkins

# install JVM
RUN apt update && apt install default-jdk-headless wget curl gnupg git procps unzip bzip2 xz-utils python -y && java --version

# install Jenkins
RUN wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | apt-key add -
RUN echo "deb http://pkg.jenkins.io/debian-stable binary/" > /etc/apt/sources.list.d/jenkins.list; \
	apt-get update && apt-get install jenkins -y;

# Prepare scripts
COPY batch-install-jenkins-plugins.sh entrypoint.sh /var/lib/jenkins/
RUN chmod +x /var/lib/jenkins/batch-install-jenkins-plugins.sh /var/lib/jenkins/entrypoint.sh

# install plugins
USER jenkins
RUN mkdir -p /var/lib/jenkins/.jenkins/plugins
COPY plugins /var/lib/jenkins/
RUN /var/lib/jenkins/batch-install-jenkins-plugins.sh --plugins /var/lib/jenkins/plugins --plugindir /var/lib/jenkins/.jenkins/plugins

ENTRYPOINT ["/bin/bash","-c"]
CMD ["/var/lib/jenkins/entrypoint.sh"]